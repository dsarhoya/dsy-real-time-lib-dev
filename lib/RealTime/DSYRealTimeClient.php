<?php

namespace DSY\RealTime;

use GuzzleHttp\Client;

class DSYRealTimeClient
{
    private $baseUri = null;
    private $accessToken = null;

    /**
     * @var Client
     */
    private $client = null;

    public function __construct($baseUri, $accessToken)
    {
        $this->accessToken = $accessToken;
        $this->baseUri = $baseUri;
        $this->client = new Client([
            'headers' => ['X-access-token' => $this->accessToken],
            'base_uri' => $this->baseUri,
        ]);
    }

    /**
     * @return DynamicDataResponse
     */
    public function create(array $data)
    {
        $response = $this->client->post('/dynamic-data', [
            'json' => $data,
        ]);

        $res = json_decode($response->getBody(), true);

        return new DynamicDataResponse($res['id'], $res['slug']);
    }

    /**
     * @return null
     */
    public function update($id, array $data)
    {
        $this->client->patch("/dynamic-data/$id", [
            'json' => $data,
        ]);
    }
}
